from machine import Pin
from machine import ADC
from nodemcu_gpio_lcd import GpioLcd
import network
import ubinascii
from time import sleep

lcd = None
WLANS = None
KEY_NONE = 0
KEY_RIGHT = 1
KEY_UP = 2
KEY_DOWN = 3
KEY_LEFT = 4
KEY_SEL = 5

def getlcd():
    rs = Pin(0)
    en = Pin(2)
    d7 = Pin(13)
    d6 = Pin(12)
    d5 = Pin(14)
    d4 = Pin(4)
    return GpioLcd(rs_pin=rs, enable_pin=en, d4_pin=d4, d5_pin=d5, d6_pin=d6, d7_pin=d7, num_lines=2, num_columns=16)


def getkey(value):
    if value < 100:
        return KEY_RIGHT
    if value < 200:
        return KEY_UP
    if value < 500:
        return KEY_DOWN
    if value < 700:
        return KEY_LEFT
    if value < 1018:
        return KEY_SEL
    return KEY_NONE


def menu():
    global WLANS, lcd
    adc = ADC(0)
    pos = 0
    info = 0
    while True:
        key = getkey(adc.read())
        if key == KEY_NONE:
            sleep(.1)

        elif key == KEY_LEFT:
            pos -= 1
            if pos < 0:
                pos = len(WLANS)-1
            if pos < 0:
                lcd.clear()
                lcd.putstr("new Scan")
                WLANS = network.WLAN(network.STA_IF).scan()
            showinfo(pos, info)

        elif key == KEY_RIGHT:
            pos += 1
            if pos >= len(WLANS):
                pos = 0
            showinfo(pos, info)

        elif key == KEY_UP:
            info -= 1
            if info < 0:
                info = 4
            showinfo(pos, info)

        elif key == KEY_DOWN:
            info += 1
            if info > 5:
                info = 0
            showinfo(pos, info)
        else:
            lcd.clear()
            ap_if = network.WLAN(network.AP_IF)
            ap_if.active(True)
            lcd.putstr("AP MODE\n")
    
            ap_if.config(essid=WLANS[pos][0], authmode=network.AUTH_OPEN)
            sleep(2)
            lcd.putstr(str(WLANS[pos][0])[2:-1])
            sleep(2)
            lcd.clear()
            lcd.putstr(str(WLANS[pos][0])[2:-1])
            net = str(WLANS[pos][0])[2:-1]
            while True:
                sleep(2)
                lcd.clear()
                lcd.putstr(net[:15]+'\n')
                lcd.putstr(str(ap_if.status()))
                sleep(1)
                for i in ap_if.ifconfig():
                    lcd.clear()
                    lcd.putstr(net[:15]+'\n')
                    lcd.putstr(i)
                    sleep(1)
                lcd.clear()
                lcd.putstr(net)

def showinfo(pos, info):
    global WLANS, lcd
    data = ""
    data = str(WLANS[pos][0])[2:-1][:15]+'\n'
    lcd.clear()
    if info == 0:
        data = str(WLANS[pos][info])[2:-1]
    elif info == 1:
        data += str(ubinascii.hexlify(WLANS[pos][info]))[2:-1]
    elif info == 2:
        data += "Channel # " + str(WLANS[pos][info])

    elif info == 3:
        data += "Sig-Lvl: " + str(WLANS[pos][info]) + "dBm"

    elif info == 4:
        if WLANS[pos][info] == 0:
            data += "  open"
        elif WLANS[pos][info] == 1:
            data += "  WEP"
        elif WLANS[pos][info] == 2:
            data += " WPA-PSK"
        elif WLANS[pos][info] == 3:
            data += "  WPA2 PSK"
        elif WLANS[pos][info] == 4:
            data += "WPA & WPA2 PSK"

    elif info == 5:
        if WLANS[pos][info] == 0:
            data += " visible "    
        else:
            data += " hidden "

    lcd.putstr(data)
    sleep(1) 


def main():
    global WLANS, lcd
    lcd = getlcd()
    lcd.clear()
    lcd.putstr("scanning")
    sta_if = network.WLAN(network.STA_IF)

    lcd.clear()
    lcd.putstr("scanning .")
    sta_if.active(True)
    sleep(.25)

    lcd.clear()
    lcd.putstr("scanning . .")
    WLANS = sta_if.scan()

    lcd.clear()
    lcd.putstr(str(len(WLANS))+" WLANs\n in Range")
    sleep(1)
    menu()

if __name__ == "__main__":
    main()