def info():
    print("""This little function-collection can help with simple file operations
function ^  arguments  ^ description
 cat     | fname,pprint   | reads the file ''fname'', ''pprint'' = print it?
 cd      | dname          | change directory to ''dname'' (relative & absolute)
 cp      | old,new        | copy the ''old''-file to ''new''
 deltree | dname          | deletes a subdirectory recursively
 df      | dname          | shows free diskspace of fs behind ''dname''-dir
 info    |                | this text
 ls      | dname          | list files and dirs in the directory ''dname''
 mem     | free           | prints free heap, free = run GC
 mkdir   | dname          | creates a the directory named ''dname''
 mv      | old,new        | moves file from ''old'' to ''new''
 reset   |                | machine.reset()
 rm      | fname          | removes file or dir with name ''fname''
 pwd     |                | print working directory
 save    | fname,data,app | save ''data'' to file ''fname''. ''app'' = append?
 speed   | freq           | returns or set the cpu speed (freq is optional)

 rfkill  | enable         | enable/disable WLan
 wconn   | essid,password | connect to essid & use password
 wscan   |                | list all available wlan ssids
 ifconfig| pprint         | print interface config, pprint = print it?
 wlanap  | essid,psk,chan | open a WLAN-AP essid=name psk=WPA2-Passwd chan=nel
Version 20190103""")


def reset():
    __import__("machine").reset()


def cat(fname, pprint=False):
    x = open(fname)
    fname = x.read()
    x.close()
    if pprint:
        print(fname)
    else:
        return fname


def save(fname, data, append=False):
    if append:
        fh = open(fname, "a")
    else:
        fh = open(fname, "w")
    data = fh.write(data)
    fh.flush()
    fh.close()
    return data


def mkdir(dname):
    return __import__("os").mkdir(dname)


def ls(dname=""):
    try:
        for i in __import__("os").ilistdir(dname):
            if i[1] == 0x8000:
                print("f[", flen(dname+"/"+i[0]), "byte ]\t ", i[0])
            else:
                print("d[",
                      len(__import__("uos").listdir(dname+"/"+i[0])),
                      "files]\t ", i[0])
    except:
        for i in __import__("os").listdir(dname):
            print(i)


def flen(fname):
    x = open(fname, "r")
    fname = x.seek(__import__("flashbdev").size)
    x.close()
    return fname


def cd(dname):
    __import__("os").chdir(dname)


def rm(fname):
    try:
        __import__("os").remove(fname)
    except:
        __import__("os").rmdir(fname)

def deltree(fname):
    cd(fname)
    for i in __import__("os").listdir():
        if __import__("os").stat(i)[0] == 16384:
            deltree(i)
        else:
            rm(i)
    cd("..")
    rm(fname)


def pwd():
    return __import__("os").getcwd()


def mv(old, new):
    __import__("os").rename(old, new)


def cp(old, new):
    n = open(new, "w")
    n.write(cat(old))
    n.flush()
    n.close()


def df(dname=None):
    if dname is None:
        dname = pwd()
    print("diskfree for:", dname)
    if 'statvfs' in dir(__import__("os")):
        x = __import__("os").statvfs(dname)
    else:
        # PyCom
        x = __import__("os").stat(dname)
    try:
        print("flash-size: ", __import__("flashbdev").size, "byte")
    except:
        try:
            # PyCom
            print(" os.getfree: ", __import__("os").getfree(dname), "Kbytes")
        except:
            pass
    print("by blocks\t(blocksize: %u)" % (x[0],))
    print(" -dsk-size:\t%ublocks\t%ubyte" % (x[2], (x[2] * x[1])))
    print(" -dsk-free:\t%ublocks\t%ubyte" % (x[4], (x[4] * x[0])))
    print(" -dsk-used:\t%ublocks\t%ubyte" % (x[3], (x[0] * (x[2] - x[3]))))


def mem(free=False):
    if free:
        free = __import__("gc").isenabled()
        __import__("gc").disable()
        __import__("gc").collect()
        if free:
            __import__("gc").enable()
    __import__("micropython").mem_info()
    return __import__("gc").mem_free()


def turbo(on=True):
    __import__("machine").freq(16 * 10**7 if on else 8*10**7)


def speed(speed=0):
    if speed > 0:
        return __import__("machine").freq(speed)
    return __import__("machine").freq()

### -- snip -- head -n 140 >> fs2.py

def rfkill(enable=False):
    __import__("network").WLAN().active(enable)

def wscan(pprint=0):
    old = __import__("network").WLAN().active()
    __import__("network").WLAN().active(True)
    retval = __import__("network").WLAN().scan()
    if pprint:
        for w in retval:
            print(w[0].decode())
            if pprint > 1:
                authinfo = ["open", "WEP", "WPA", "WPA2", "WPA/WPA2"]
                print(" '-> Channel", w[2])
                print(" '-> Auth   ", authinfo[w[4]])
            if pprint > 2:
                print(" '-> BSSID  ", w[1])
                print(" '-> RSSI   ", w[3])
                print(" '->", "visible" if w[5] else "hidden")
            if pprint > 1:
                print()
    __import__("network").WLAN().active(old)
    return retval

def wconn(ssid=None,pwd=None):
    __import__("network").WLAN().active(True)
    if ssid is None:
        ssid = wscan(1)
        ssid = input("Enter SSID: ")
    if pwd is None:
        pwd = input("Password: ")
    __import__("network").WLAN().connect(ssid,pwd)

def ifconfig(pprint=0):
    retval = __import__("network").WLAN().ifconfig()
    if pprint:
        print("IPv4:   ", retval[0])
        print("netmask:", retval[1])
        print("Gateway:", retval[2])
        print("DNS:    ", retval[3])
    return retval

def wlanap(ssid, psk=None, channel=1):
    retval = __import__("network").WLAN(__import__("network").AP_IF)
    if psk:
        retval.config(password=psk)
        retval.config(authmode=__import__("network").AUTH_WPA2_PSK)
    retval.config(essid=ssid, channel=channel, hidden=False)
    retval.active(True)
    return retval
