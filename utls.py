def random(max=255):
    from uos import urandom
    tmp = urandom(int(max/256)+1)
    retval = 0
    for x in tmp:
        retval += int(x)
    return retval % (max + 1)


def zfill(txt, flen=8, fill=b'0'):
    txt = txt[:8]
    return fill * (flen - len(txt)) + txt


def int2bits(value, bits=8):
    retval = []
    for x in zfill(bin(value)[2:], bits, b'0'):
        retval.append(x == 49)
    return retval


def tm1637(value, data, clk):
    value = int2bits(value)
    for x in value:
        clk.value(0)
        if x:
            data.value(1)
        else:
            data.value(0)
        clk.value(1)
