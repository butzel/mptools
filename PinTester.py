def blink(pin):
    p = __import__("machine").Pin(pin, __import__("machine").Pin.OUT)
    while 1:
        p.value(0)
        __import__("time").sleep(.25)
        p.value(1)
        __import__("time").sleep(.3)


def read(adc=0, wait=.75):
    while 1:
        print(__import__("machine").ADC(adc).read())
        __import__("time").sleep(wait)


def pulse(pin):
    p = __import__("machine").Pin(pin, __import__("machine").Pin.OUT)
    pwm = __import__("machine").PWM(p)
    pwm.freq(1000)
    while 1:
        for i in range(10, 1000, 50):
            __import__("time").sleep(.125)
            pwm.duty(i)
        for i in range(1000, 10, -50):
            __import__("time").sleep(.07)
            pwm.duty(i)
