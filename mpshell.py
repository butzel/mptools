# -*- coding: UTF-8 -*-
__author__ = 'butzel'
__license__ = "GPLv2: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html"

class mpobject(object):

    def __getattribute__(self, name):
        return getattr(self, name) 

    def __setattribute__(self, name):
        return setattr(self, name) 


class cmd(mpobject):
    """ simple shell class

    new commands:
        create an inherited class of this 
        and append a method named "cmd_" + yourcommand

    out(text) - write to std-out
    err(text) - write to error-out
    read(len) - read from std-in

    this shell supports simple piping (">" "<" "|")
    """
    def __init__(self):
        self._err2 = self._write2console
        self._out2 = self._write2console
        self._in = self._readstdin
        self._pipe = ""
        self._outfile = ""
        self._errfile = ""
        self._loop = True

    def out(self, text):
        """ use this method for
        writing on  std-out """
        self._out2(text)

    def err(self, text):
        """ use this method for
        writing on error-out """
        self._err2(text)

    def read(self, bytelen=1, echo=True):
        """ read from std-in """
        retval = ""
        while len(retval) < bytelen:
            retval += self._in(bytelen)
            if echo:
                self.out(retval[-1])
        return retval

    def readline(self, until="\n", echo=True):
        retval = " "
        while not retval[-1] == until:
            retval += self.read(1)
        return retval[1:]


    def _readstdin(self, bytelen):
        return __import__("sys").stdin.read(bytelen)

    def _readpipe(self):
        return self._pipe

    def _write2console(self, data):
        print(data, end="")
        return True

    def _write2errfile(self, data):
        return self.write2file(data, self._errfile)

    def _write2outfile(self, data):
        return self.write2file(data, self._outfile)

    def _write2file(self, data, fname):
        try:
            fh = open(fname, "r")
            retval = fh.write(data)
            fh.close()
            return retval
        except:
            return False

    def _write2pipe(self, data):
        self._pipe = data
        return True

    def parse(self, cmdline, piped=0):
        cmd = []
        cnt = piped
        
        for x in cmdline.split():
            cnt += 1
            if piped:
                self._in = self._readpipe
            else:
                self._in = self._readstdin
            if x == ">":
                pass
            elif x == "<":
                pass
            elif x == "|":
                self._out2 = self._write2pipe
                piped = cnt
                break
            else:
                cmd.append(x)
        if cmd:
            self.do(cmd)

    def do(self, cmd):
        try:
            func = self.__getattribute__("cmd_" + cmd[0])
            return func(cmd)
        except AttributeError:
            pass # not implemented 
    
    def run(self):
        while self._loop:
            self.out("\nµPython:>")
            self.parse(self.readline())

    @property
    def loop(self):
        return self._loop

    def cmd_echo(self, cmd):
        """
        print text to std-out

        try:
          echo print me
        """
        for i in cmd[1:]:
            self.out(i+" ")
        self.out("\n")

    def cmd_exit(self, cmd):
        """
        kill me
        """
        self._loop = False

    def cmd_rev(self, cmd):
        """ print reversed """
        retval = ""
        for i in cmd[1:]:
            retval += i+" "
        self.out(retval.strip()[::-1]+"\n")

    def cmd_wlan(self, cmd):
        # need a class member wlan...
        # self.wlan = __import__(net).Wlan()
        if len(cmd)>1:
            if cmd[1] == "scan":
                print("scan")
                wlan = __import__("network").WLAN(0)
                if wlan.isactive():
                    print("ena")
                    self.out("ch ^ auth ^ strength ^ name \n")
                    for w in wlan.scan():
                        self.out(w[2] + " | " )
                        if w[4] == 0:
                            self.out(" open | " )
                        elif w[4] == 1: 
                            self.out("  WEP | " )
                        elif w[4] == 2: 
                            self.out("  WPA | " )
                        else: 
                            self.out(" WPA2 | " )
                        self.out(w[3] + "\t " + w[1] + " " + w[2]+ "\n")
                else:
                    print("ups")
                    self.out("please enable wlan first\n")
            elif cmd[1] == "start":
                __import__("network").WLAN(0).active(True)
            elif cmd[1] == "stop":
                __import__("network").WLAN(0).active(False)
            elif cmd[1] == "status":
                if __import__("network").WLAN(0):
                    self.out("WLAN is active\n")
                else:
                    self.out("WLAN is disabled\n")
            elif cmd[1] == "connect":
                
                wlan = __import__("network").WLAN(0)
                if not wlan.isactive():
                    self.out("please enable wlan first\n")
                    return
                if len(cmd) > 2:
                    name = cmd[2]
                if len(cmd) == 3:
                    pwd = cmd[3]
                else:
                    pwd = self.readline()
                if name and pwd:
                    wlan.connect(name, pwd)
