def circle(fbuf,x,y, radius, clr=1):
    fbuf.pixel(x,y+radius, clr)
    fbuf.pixel(x,y-radius, clr)
    fbuf.pixel(x+radius,y, clr)
    fbuf.pixel(x-radius,y, clr)
    sub = 1 - radius
    subx = 0
    suby = radius*-2
    xx = 0
    yy = radius
    while xx<yy:
        if not sub<0:
            yy-=1
            suby +=2
            sub += suby
        xx+=1
        subx+=2
        sub+=1+subx
        fbuf.pixel(x+xx,y+yy,clr)
        fbuf.pixel(x+xx,y-yy,clr)
        fbuf.pixel(x+yy,y+xx,clr)
        fbuf.pixel(x+yy,y-xx,clr)
        fbuf.pixel(x-xx,y-yy,clr)
        fbuf.pixel(x-xx,y+yy,clr)
        fbuf.pixel(x-yy,y-xx,clr)
        fbuf.pixel(x-yy,y+xx,clr)

def line(fbuf,x,y,xend,yend, clr=1):
    xlen = abs(x-xend)
    ylen = abs(y-yend)*-1
    xdir = 1 if x < xend else -1
    ydir = 1 if y < yend else -1
    sub = xlen+ylen
    while  x!=xend or y!=yend:
        fbuf.pixel(x,y,clr)
        if sub+sub > ylen:
            x+=xdir
            sub+=ylen
        if sub+sub < xlen:
            sub+=xlen
            y+=ydir